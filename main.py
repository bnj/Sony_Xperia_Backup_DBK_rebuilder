#-*- coding: utf-8 -*-
#  File System rebuilder for Sony Xperia DBK backup file
#  Copyright (C) 2018  Benjamin - EWFT <benjamin@ewft.org>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software Foundation,
#   Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
#


import os
import shutil
from xml.etree import ElementTree


# Output folder for the reconstruction
top_root = "../result2/"

# Path to the FileSystem.xml
FileSystemXML = "../FileSystem.xml"

# Path to the Content Folder
ContentFolder = "../Content"

# Dry Run
DryRun = True


def folder_walk(folder, indent, root):
	indent += "\t"
	for folder_un in folder.findall('Folder'):
		print indent + u"⊢ " + folder_un.get('Name')
		folderpath = os.path.join(root, folder_un.get('Name'))
		if DryRun:
			print indent + "  make dir " + folderpath
		else:
			os.mkdir(folderpath)
		folder_walk(folder_un, indent, folderpath)
	for file_elmt in folder.findall('File'):
		if file_elmt.get('Content-Id') is not None:
			print indent + u"⊢ " + file_elmt.get('Name') + " - " + file_elmt.get('Content-Id')
			if DryRun:
				print indent + "  move " + os.path.join(ContentFolder, file_elmt.get('Content-Id')) + " to " + os.path.join(root, file_elmt.get('Name'))
			else:
				shutil.move(os.path.join(ContentFolder, file_elmt.get('Content-Id')), os.path.join(root, file_elmt.get('Name')))


doc = ElementTree.parse(FileSystemXML).getroot()

if DryRun:
	print "make dir " + top_root
else:
	os.mkdir(top_root)

for content in doc.findall('Volume')[0].findall('Content'):
	print content.tag
	folder_walk(content, " ", top_root)

