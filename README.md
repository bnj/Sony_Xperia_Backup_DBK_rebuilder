# File System rebuilder for Sony Xperia DBK backup file


Sony Xperia backup file, with dbk extension, are in fact zip archive.
But all files are stored in a same an unique "Content" folder, with as a filename a "Content-Id".
And a FileSystm.xml file contains the folder hierarchy and the correspondance between real name and Content-Id.


This script reconstruct the filesystem, by recreating the folder hierarchy, renaming the file with their correct name and moving them at their location in the folder hierarchy.

## Requierment

Python 2.7


## Usage

Open main.py 

Setup Parameters

```python

# Output folder for the reconstruction
top_root = "../result2/"

# Path to the FileSystem.xml
FileSystemXML = "../FileSystem.xml"

# Path to the Content Folder
ContentFolder = "../Content"

# Dry Run
DryRun = True
```

run script with

``` python2 main.py ```
 
 




